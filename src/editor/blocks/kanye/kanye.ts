import { NodeBlock, Slots, slots, tripetto, editor } from "tripetto";
import * as ICON from "./kanye.svg";

@tripetto({
    type: "node",
    identifier: "kanye",
    label: "Kanye Quote",
    icon: ICON
})
export class KanyeBlock extends NodeBlock {
    @slots
    defineSlot(): void {
        this.slots.static({
            type: Slots.String,
            reference: "kanye-quote",
            label: "Kanye quote"
        });
    }

    @editor
    defineEditor(): void {
        this.editor.name(false, false, "Name");
        this.editor.visibility();
    }
}
