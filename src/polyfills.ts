import * as smoothscroll from "smoothscroll-polyfill";

// Mobile Safari reports it supports smooth scrolling. That's not the case, so force the polyfill on mobile safari. Sigh.
if (navigator.userAgent.match(/(iPod|iPhone|iPad)/) && navigator.userAgent.match(/AppleWebKit/)) {
    // @ts-ignore
    window.__forceSmoothScrollPolyfill__ = true;
}

smoothscroll.polyfill();
