import * as React from "react";
import * as Tripetto from "tripetto-collector";
import { Blocks } from "./helpers/blocks";
import { ICollectorProps } from "./helpers/interfaces/props";
import "./blocks";
import "./collector.scss";

export class Collector extends React.PureComponent<ICollectorProps> {
    readonly blocks = new Blocks(this, this.props.definition);
    readonly scroller: React.RefObject<HTMLDivElement> = React.createRef();
    readonly state = {
        paddingTop: 0,
        paddingBottom: 0,
        activeBlock: "",
        isRendered: false,
        isAtBegin: true,
        isAtEnd: false
    };

    render(): React.ReactNode {
        return (
            <>
                {this.blocks.render() ||
                    (this.blocks.status === "finished" && (
                        <section className="container">
                            <div className="row">
                                <div className="col">
                                    <div className="text-center mt-5">
                                        <h3>❤ All done</h3>
                                        <p className="text-secondary">
                                            This is a demo. No data is sent to a server. Below is a summary of the collected data for demo
                                            purposes. Open the developer console to see more data export options.
                                        </p>
                                    </div>
                                    {this.blocks.instance && (
                                        <div className="mt-2">
                                            <table className="table">
                                                <tbody>
                                                    <tr>
                                                        <th scope="col">Name</th>
                                                        <th scope="col">Value</th>
                                                    </tr>
                                                    {Tripetto.Export.fieldsFilled(this.blocks.instance).fields.map(
                                                        (pField: Tripetto.Export.IField) => (
                                                            <tr key={pField.key}>
                                                                <td>{pField.name}</td>
                                                                <td>{pField.string}</td>
                                                            </tr>
                                                        )
                                                    )}
                                                </tbody>
                                            </table>
                                        </div>
                                    )}
                                </div>
                            </div>
                        </section>
                    )) ||
                    (this.blocks.status === "paused" && (
                        <section className="container">
                            <div className="row">
                                <div className="col">
                                    <div className="text-center mt-5">
                                        <h3>⏸ Paused</h3>
                                        <p className="text-secondary">
                                            Hit browser refresh to resume it. Normally you should store the resume data somewhere, so you
                                            can use it to restore the session later on. For this demo we've saved the resume data to the
                                            local store of your browser.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </section>
                    )) || (
                        <section className="container">
                            <div className="row">
                                <div className="col">
                                    <div className="text-center mt-5">
                                        <h3>⏹ Stopped</h3>
                                        <p className="text-secondary">Click play to start it.</p>
                                    </div>
                                </div>
                            </div>
                        </section>
                    )}
            </>
        );
    }

    /** Bind to some events. */
    componentDidMount(): void {
        this.blocks.onChange = () => {
            // Since the collector has the actual state, we need to update the component.
            // We are good React citizens. We only do this when necessary!
            this.forceUpdate();
        };

        // When ready, invoke our `onFinish` callback.
        this.blocks.onFinish = (instance: Tripetto.Instance) => {
            if (this.props.onFinish) {
                this.props.onFinish(instance);
            }
        };
    }

    /** Reloads with a new definition. */
    reload(definition: Tripetto.IDefinition): void {
        this.blocks.reload(definition);
    }
}
