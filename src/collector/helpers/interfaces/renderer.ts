import { NodeBlock } from "tripetto-collector";
import { IBlockHelper } from "./helper";

export interface IBlockRenderer extends NodeBlock {
    render: (h: IBlockHelper) => React.ReactNode;
}
