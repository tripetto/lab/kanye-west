import { IDefinition, ISnapshot, Instance } from "tripetto-collector";

export interface ICollectorProps {
    /** Specifies the definition to run. */
    readonly definition: IDefinition;

    /** Specifies a function that is invoked when the collector is finished. */
    readonly onFinish?: (instance: Instance) => void;
}
