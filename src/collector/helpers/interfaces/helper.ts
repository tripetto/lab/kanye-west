import * as React from "react";

export interface IBlockHelper {
    /** Parsed markdown name. */
    name: (required?: boolean) => React.ReactNode;

    /** Parsed markdown description. */
    description: React.ReactNode;

    /** Parsed markdown explanation. */
    explanation: (label?: string) => React.ReactNode;

    /** Parsed markdown placeholder. */
    placeholder: string;

    /** Tab-index for the block. */
    tabIndex: number;

    /** Specifies if the block is active. */
    isActive: boolean;

    /** Specifies if the block validation failed. */
    isFailed: boolean;

    /** Move to the next block immediately. */
    next: () => void;

    /** Move to the next block after a delay (so there is some time for a nice animation). */
    nextAfterDelay: () => void;

    /** Retrieves the keystroke function. */
    keystroke: (on: (key: number, shift: boolean) => boolean | void) => void;

    /** Specifies the element that should receive focus. */
    setFocus: (ref: HTMLElement | null) => void;
}
