import * as React from "react";
import * as Tripetto from "tripetto-collector";
import { Collector } from "../collector";
import { IBlockRenderer } from "./interfaces/renderer";
import { markdownToJSX } from "./markdown";

export class Blocks extends Tripetto.Collector<IBlockRenderer> {
    private component: Collector;
    private handle = 0;
    private keyHandler?: (key: number, shift: boolean) => boolean | void;

    constructor(component: Collector, definition: Tripetto.IDefinition) {
        super(definition, "progressive", true);

        this.component = component;
    }

    /** Find out which block to activate based on the scroll position. */
    private findActiveBlock(): void {
        if (this.handle) {
            cancelAnimationFrame(this.handle);
        }

        // Wait a frame before reading from the dom to avoid jank.
        this.handle = requestAnimationFrame(() => {
            const scroller = this.component.scroller.current;

            this.handle = 0;

            if (!scroller) {
                this.handle = requestAnimationFrame(() => this.findActiveBlock());

                return;
            }

            const blocks = scroller.firstElementChild;

            if (blocks) {
                const scrollRect = scroller.getBoundingClientRect();
                const height = scrollRect.height;
                const median = scrollRect.top + height / 2;
                let size = 0;
                let sizeFirst = 0;
                let sizeLast = 0;
                let activeBlock = "";
                let activeIndex = 0;
                let count = 0;
                const padding = {
                    top: 0,
                    bottom: 0
                };

                Tripetto.each(blocks.children, (block: Element | null) => {
                    const key = block!.getAttribute("data-key");
                    const rect = block!.getBoundingClientRect();

                    if (rect.height > 0) {
                        if (Tripetto.Num.inRange(median, rect.top, rect.bottom)) {
                            activeBlock = key || "";
                            activeIndex = count;
                        }

                        size += rect.height;

                        sizeFirst = sizeFirst || rect.height;
                        sizeLast = rect.height;

                        count++;
                    }
                });

                padding.top = Tripetto.Num.floor(Tripetto.Num.max(height - sizeFirst, 0) / 2);
                padding.bottom = Tripetto.Num.max(height - padding.top - size, 0);

                if (padding.top + size > height) {
                    padding.bottom += Tripetto.Num.floor(Tripetto.Num.max(height - sizeLast, 0) / 2);
                } else {
                    padding.bottom += padding.top + size - sizeLast / 2 - height / 2;
                }

                this.component.setState({
                    paddingTop: padding.top,
                    paddingBottom: padding.bottom,
                    isRendered: true,
                    activeBlock: activeBlock,
                    isAtBegin: activeIndex === 0,
                    isAtEnd: activeIndex + 1 === count
                });
            }
        });
    }

    /** Activates the block with the given key. */
    private activate(key: string): void {
        const scroller = this.component.scroller.current;

        if (scroller) {
            const blocks = scroller.firstElementChild;

            if (blocks) {
                const block = Tripetto.findFirst(blocks.children, (el: Element | null) => el!.getAttribute("data-key") === key);

                if (block) {
                    const innerRect = block.getBoundingClientRect();
                    const outerRect = scroller.getBoundingClientRect();
                    const diff = innerRect.top + innerRect.height / 2 - (outerRect.top + outerRect.height / 2);
                    const from = scroller.scrollTop;
                    const to = from + diff - Tripetto.Num.max(outerRect.top - (innerRect.top - diff), 0);

                    if (scroller.scrollTo) {
                        scroller.scrollTo({
                            left: 0,
                            top: to,
                            behavior: "smooth"
                        });
                    } else {
                        scroller.scrollTop = to;
                    }
                }
            }
        }
    }

    /** Move to another block in order to activate it. */
    private move(direction: "up" | "down"): void {
        const scroller = this.component.scroller.current;

        if (scroller) {
            const blocks = scroller.firstElementChild;

            if (blocks) {
                let dest = "";
                let active = false;

                Tripetto.each(blocks.children, (block: Element | null) => {
                    const key = block!.getAttribute("data-key");
                    const rect = block!.getBoundingClientRect();

                    if (key && rect.height > 0) {
                        if (key === this.component.state.activeBlock) {
                            active = true;
                        } else {
                            switch (direction) {
                                case "up":
                                    if (!active) {
                                        dest = key;
                                    }

                                    break;
                                case "down":
                                    if (active) {
                                        dest = dest || key;
                                    }

                                    break;
                            }
                        }
                    }
                });

                this.activate(dest);
            }
        }
    }

    /** Handle key down. */
    private keyDown(event: React.KeyboardEvent): void {
        if (this.keyHandler) {
            const bReturn = this.keyHandler(event.keyCode, event.shiftKey);

            if (Tripetto.isBoolean(bReturn)) {
                if (bReturn) {
                    event.preventDefault();
                }

                return;
            }
        }

        switch (event.keyCode) {
            case 38: // Up
            case 40: // Down
                this.move(event.keyCode === 38 ? "up" : "down");

                event.preventDefault();

                break;
            case 13: // Enter
                if (this.component.state.activeBlock === "complete") {
                    this.finish();
                } else {
                    this.move("down");
                }

                event.preventDefault();

                break;
        }
    }

    /** Render the blocks. */
    render(): React.ReactNode {
        const storyline = this.storyline;
        let tabIndex = 0;

        this.keyHandler = undefined;

        if (!storyline) {
            return undefined;
        }

        this.findActiveBlock();

        return (
            <>
                <div
                    tabIndex={++tabIndex}
                    ref={this.component.scroller}
                    onScroll={() => this.findActiveBlock()}
                    onKeyDown={(ev: React.KeyboardEvent) => this.keyDown(ev)}
                    className={this.component.state.isRendered ? "" : "loading"}
                >
                    <div
                        className="container"
                        style={{
                            paddingTop: `${this.component.state.paddingTop}px`,
                            paddingBottom: `${this.component.state.paddingBottom}px`
                        }}
                    >
                        {storyline.map((moment: Tripetto.Moment<IBlockRenderer>) =>
                            moment.nodes.map((node: Tripetto.IObservableNode<IBlockRenderer>) => {
                                const isActive = this.component.state.activeBlock === node.key;

                                return (
                                    <div key={node.key} data-key={node.key} className={`row ${isActive ? "active" : ""}`}>
                                        <div className="col">
                                            {node.block ? (
                                                node.block.render({
                                                    name: (required?: boolean) =>
                                                        Tripetto.castToBoolean(node.props.nameVisible, true) && (
                                                            <h2>
                                                                {markdownToJSX(node.props.name || "...", node.context)}
                                                                {required && <span className="required" />}
                                                            </h2>
                                                        ),
                                                    get description(): React.ReactNode {
                                                        return (
                                                            node.props.description && (
                                                                <p className="text-secondary">
                                                                    {markdownToJSX(node.props.description, node.context)}
                                                                </p>
                                                            )
                                                        );
                                                    },
                                                    explanation: (label?: string) =>
                                                        node.props.explanation && (
                                                            <small className="form-text text-secondary" id={label}>
                                                                {markdownToJSX(node.props.explanation, node.context)}
                                                            </small>
                                                        ),
                                                    get placeholder(): string {
                                                        return Tripetto.markdownifyToString(
                                                            node.props.placeholder || "",
                                                            node.context,
                                                            "..."
                                                        );
                                                    },
                                                    get tabIndex(): number {
                                                        return ++tabIndex;
                                                    },

                                                    get isActive(): boolean {
                                                        return isActive;
                                                    },
                                                    get isFailed(): boolean {
                                                        return !isActive && node.isFailed;
                                                    },
                                                    next: () => this.move("down"),
                                                    nextAfterDelay: () => setTimeout(() => this.move("down"), 500),
                                                    keystroke: (on: (key: number, shift: boolean) => boolean | void) => {
                                                        if (this.component.state.activeBlock === node.key) {
                                                            this.keyHandler = on;
                                                        }
                                                    },
                                                    setFocus: (ref: HTMLElement | null) => {
                                                        if (!ref) {
                                                            return;
                                                        }

                                                        if (isActive) {
                                                            let active: Element | null = document.activeElement;
                                                            let allowed = active ? false : true;

                                                            while (active && !allowed) {
                                                                if (active === this.component.scroller.current) {
                                                                    allowed = true;
                                                                }

                                                                active = active.parentElement;
                                                            }

                                                            if (allowed) {
                                                                ref.focus();
                                                            }
                                                        } else {
                                                            const current = document.activeElement;

                                                            if (current && current.isSameNode(ref)) {
                                                                ref.blur();
                                                            }
                                                        }
                                                    }
                                                })
                                            ) : (
                                                <div className="static" onClick={() => this.move("down")}>
                                                    {node.props.nameVisible && (
                                                        <h2>{markdownToJSX(node.props.name || "...", node.context)}</h2>
                                                    )}
                                                    {node.props.description && (
                                                        <p className="text-secondary">
                                                            {markdownToJSX(node.props.description, node.context, true)}
                                                        </p>
                                                    )}
                                                    <div className={isActive ? "active" : ""}>
                                                        <button className="btn btn-lg btn-primary" onClick={() => this.move("down")}>
                                                            Continue
                                                            <i className="fas fa-level-down-alt fa-fw fa-rotate-90 ml-2" />
                                                        </button>
                                                    </div>
                                                </div>
                                            )}
                                        </div>
                                        <div className="activator" onClick={() => this.activate(node.key)} />
                                    </div>
                                );
                            })
                        )}

                        {storyline.isFinishable && (
                            <div
                                data-key="complete"
                                className={`row complete ${this.component.state.activeBlock === "complete" ? " active" : ""}`}
                            >
                                <div className="col">
                                    <button className="btn btn-lg btn-success" onClick={() => storyline.finish()}>
                                        Complete
                                    </button>
                                </div>
                            </div>
                        )}

                        {storyline.isEmpty && (
                            <div className="row active">
                                <div className="col">
                                    <div className="text-center mt-5">
                                        <h3>👋 Nothing to show here yet</h3>
                                        <p className="text-secondary">Add blocks to the form first to get the magic going.</p>
                                    </div>
                                </div>
                            </div>
                        )}
                    </div>
                </div>
            </>
        );
    }
}
