import * as React from "react";
import * as Tripetto from "tripetto-collector";
import { Text } from "tripetto-block-text/collector";
import { IBlockRenderer } from "../../helpers/interfaces/renderer";
import { IBlockHelper } from "../../helpers/interfaces/helper";
import "./text.scss";

@Tripetto.block({
    type: "node",
    identifier: "tripetto-block-text"
})
export class TextBlock extends Text implements IBlockRenderer {
    render(h: IBlockHelper): React.ReactNode {
        return (
            <div className="form-group text">
                {h.name(this.required)}
                {h.description}
                <input
                    type="text"
                    key={this.key()}
                    required={this.required}
                    defaultValue={this.textSlot.value}
                    tabIndex={h.tabIndex}
                    placeholder={h.placeholder || "Type your answer here"}
                    maxLength={this.maxLength}
                    className={`form-control form-control-lg${h.isFailed ? " is-invalid" : ""}`}
                    aria-describedby={this.node.explanation && this.key("explanation")}
                    onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
                        this.textSlot.value = e.target.value;
                    }}
                    onFocus={(e: React.FocusEvent<HTMLInputElement>) => {
                        e.target.classList.remove("is-invalid");
                    }}
                    onBlur={(e: React.FocusEvent<HTMLInputElement>) => {
                        e.target.value = this.textSlot.string;
                        e.target.classList.toggle("is-invalid", this.isFailed);
                    }}
                    ref={(el: HTMLInputElement) => {
                        h.setFocus(el);
                        h.keystroke((key: number) => {
                            if (key === 13) {
                                h.next();

                                return true;
                            }
                        });
                    }}
                />
                <div>
                    {h.explanation(this.key("explanation"))}
                    <button disabled={this.validation !== "pass"} className="btn btn-lg btn-primary" onClick={() => h.next()}>
                        Next
                        <i className="fas fa-level-down-alt fa-fw fa-rotate-90 ml-2" />
                    </button>
                </div>
            </div>
        );
    }
}
