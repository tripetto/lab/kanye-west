import * as React from "react";
import * as Tripetto from "tripetto-collector";
import * as Superagent from "superagent";
import { IBlockRenderer } from "collector/helpers/interfaces/renderer";
import { IBlockHelper } from "collector/helpers/interfaces/helper";
import "./kanye.scss";

const DEFAULT_QUOTE = "I feel like I'm too busy writing history to read it.";

@Tripetto.block({
    type: "node",
    identifier: "kanye"
})
export class KanyeBlock extends Tripetto.NodeBlock implements IBlockRenderer {
    readonly quoteSlot = Tripetto.assert(this.valueOf<string>("kanye-quote"));

    constructor(node: Tripetto.Node, context: Tripetto.Context) {
        super(node, context);

        Superagent.get("https://api.kanye.rest").then((response: Superagent.Response) => {
            this.quoteSlot.value = response.body.quote || DEFAULT_QUOTE;
        });
    }

    render(h: IBlockHelper): React.ReactNode {
        return (
            <div tabIndex={h.tabIndex} className="kanye">
                <blockquote>
                    <p className="quotation">{this.quoteSlot.value || "..."}</p>
                    <footer>— Kanye West</footer>
                </blockquote>
                <div className={h.isActive ? "active" : ""}>
                    <button className="btn btn-lg btn-primary" onClick={() => h.next()}>
                        Continue
                        <i className="fas fa-level-down-alt fa-fw fa-rotate-90 ml-2" />
                    </button>
                </div>
            </div>
        );
    }
}
