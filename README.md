# Custom Kanye West block
Sample project to demonstrate how to create a custom block (e.g. question type) with Tripetto. Custom blocks give you the freedom to extend your survey with absolutely anything. In this example Tripetto is connected to the [Kanye West quote api](https://api.kanye.rest).

[Try demo](https://tripetto.gitlab.io/lab/kanye-west) 🕶️

[Read more on Medium](https://medium.com/tripetto/add-some-kanye-genius-to-your-survey-5f1fee97f734)

# Installation
1. [Download](https://gitlab.com/tripetto/lab/kanye-west/-/archive/master/kanye-west-master.zip) or clone the [repository](https://gitlab.com/tripetto/lab/kanye-west) to your local machine:
```bash
$ git clone https://gitlab.com/tripetto/lab/kanye-west.git
```

2. Run `npm install` inside the downloaded/cloned folder:
```bash
$ npm install
```

3. Start the test server and open the URL [http://localhost:9000](http://localhost:9000) in the browser of your choice:
```bash
$ npm test
```

# About us
If you want to learn more about Tripetto or contribute in any way, visit us at [Tripetto.com](https://tripetto.com/).
